#ifndef MY_SQL_FUNC
#define MY_SQL_FUNC

    #ifndef _mysql_h
        #error "API MYSQL REQUIS"
    #endif

    enum BOOLEAN{
        True=1,
        False=0
    };
    typedef enum BOOLEAN boolean;

    struct MYSQL_RESULT{
        boolean state;/*etat de la requete*/
        char * error_message; /*message d errer en cas d erreur*/
        boolean is_select_req;/*requete de type select ou autre*/
        int nb_row;/*nom d ligne d information retourné dans le cas d un select*/
        int nb_fields;/*nombre de champs retourné par la requete*/
        char* list_fields[10];/*liste des champs. maximum 10*/
        /*char* res[];tableau de resultat; chaque ligne represente une ocuurence*/
        MYSQL_RES* result;
    };
    typedef struct MYSQL_RESULT SQL_RES;

    SQL_RES execute_query(MYSQL* , char* );/*executer une requete*/
    MYSQL* connect_to_mysql(char* ,char*,char*,char*);/*connexion à la v=base de donnée mysql*/
    void display_result(char* title, SQL_RES res);/*affichage de liste de donnée*/
    void discret_display_result(char* title, SQL_RES res);/*affichage de liste de donnée*/
    void free_res(SQL_RES res);/*liberer une ressource*/
    void display_res_informations(SQL_RES);/*afficher les informations de l objet SQL_RES*/
    void close_connection(MYSQL *object);/*fermer la connexion*/
    void get_int_value(SQL_RES,int*,int);//la variable de type int* represente la variable qui va recevoir la valeur
    void get_string_value(SQL_RES,char*,int);
    void get_float_value(SQL_RES res, float *valeur,int value_index);
    void tolowercase(char* chaine);
#endif