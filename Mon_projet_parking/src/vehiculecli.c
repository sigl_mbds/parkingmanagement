/** 
 *@file vehiculecli.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include <mysql/mysql.h>
#include <stdio.h>
#include "vehiculecli.h"
/** 
 *@brief Fonction qui definit  les vehicules une fois en historique
*/

void vehiculecli() {
	MYSQL *conn;
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	char *server = "localhost";
	char *user = "fatim";
	char *password = "fanta"; /* set me first */
	char *database = "parkings";
	
	conn = mysql_init(NULL);
	
	/* Connection à la base de données */
	if (!mysql_real_connect(conn, server, user, password,database, 0, NULL, 0)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
	
	/* Envoi de la requete SQL  */
	if (mysql_query(conn, "select * from engins")) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
   
	res = mysql_use_result(conn);
	
	/* sortie nom de table */
	printf("\nLa liste des Document:\n");
	printf("\n| id | No | Couleur | marque |\n"); 
	while ((row = mysql_fetch_row(res)) != NULL)
		printf("| %s | %s | %s | %s |\n", row[0],row[1],row[2],row[3]);
   
	/*fermeture de la connection */
	mysql_free_result(res);
	mysql_close(conn);

}
