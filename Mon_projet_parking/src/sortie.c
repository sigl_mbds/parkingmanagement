/** 
 *@file sortie.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include<string.h>
#include<mysql/mysql.h>

#include"mysql_help_function.h"
#include"connexion_options.h"
#include "sortie.h"

void sortie(void){
	MYSQL* conn= connect_to_mysql(HOSTNAME,USERNAME,PASSWORD,PARKING_BD_NAME);
	char matricule[9];
	char code_type_place_occupe[3];
	char query[200];
	int timestamp_entrer;

	printf("Entrez le matricule du véhicule sortant svp!!!\n");
	scanf("%s",matricule);
	sprintf(query,"select UNIX_TIMESTAMP(date_entrer) as timestamp,code_type_place_occupe from stationner where mat_eng='%s' and date_entrer is not null and date_sortie is null limit 1",matricule);
	SQL_RES res=execute_query(conn,query);
	display_result("TIMESTAMP",res);
	if(res.nb_row<=0){
		printf("Il semblerait qu'il n 'y ait pas de véhicule ayant pour matricule %s garé dans le parking.\n",matricule);
	}
	else{
		get_int_value(res,&timestamp_entrer,0);
		get_string_value(res,code_type_place_occupe,1);
		printf("%d\n",timestamp_entrer);
		printf("type de place occupé : %s\n",code_type_place_occupe);

		time_t timestamp=timestamp_entrer;
		time_t now_date=time(NULL);
		double duree=difftime(now_date,timestamp);//duree en seconde
		int nb_jour=duree/(60*60*24);
		int nb_heure=(duree-(nb_jour*(60*60*24)))/(60*60);
		int temp_restant=(nb_heure*3600)+(nb_jour*(30*30*24));
		if(temp_restant>0)nb_heure++;
		printf("query %s\n",query);

		printf("Durée total en seconde:%lf\n",duree);
		printf("Nombre de jour : %d\n",nb_jour);
		printf("Nombre d'heure : %d\n",nb_heure);

		printf("query %s\n",query);
		//printf(query," ");
		printf("query %s\n",query);

		sprintf(query,"select tarif_jour, tarif_heure from type_engins where code_type='%s' limit 1",code_type_place_occupe);
		printf("query %s\n",query);
		res=execute_query(conn,query);
		display_res_informations(res);

		float tarif_jour;
		get_float_value(res,&tarif_jour,0);
		float tarif_heure;
		get_float_value(res,&tarif_heure,1);

		printf("tarif_jour : %f tarif_heure :%f",tarif_jour,tarif_heure);
		
		float facture=tarif_heure * nb_heure + tarif_jour * nb_jour;
		printf("facture : %f\n",facture);

		struct tm *tm = localtime(&now_date);
        sprintf(query,"update stationner set date_sortie='%04d-%02d-%02d %02d:%02d:%02d' where mat_eng='%s' and date_entrer is not null and date_sortie is null limit 1", tm->tm_year+1900,tm->tm_mon+1,tm->tm_mday,tm->tm_hour,tm->tm_min,tm->tm_sec,matricule);
		printf("query : %s\n",query);
		res=execute_query(conn,query);
		display_res_informations(res);
		printf("Opération éffectuée avec succès!!!\n");
	}

    time_t enter=1615073610;
    time_t out=time(NULL);
    time_t pass=(out-enter)/(60*60);
    struct tm ts_enter;
    struct tm ts_out;
    struct tm ts_pass;

    char buf1[80];
    char buf2[80];
    char buf3[80];
    ts_enter = *localtime(&enter);
    ts_out = *localtime(&out);
    ts_pass = *localtime(&pass);
    strftime(buf1, sizeof(buf1), "heure d'entrez du vehicule :%a %Y-%m-%d %H:%M:%S %Z", &ts_enter);
    strftime(buf2, sizeof(buf2), "heure de sortie du vehicule :%a %Y-%m-%d %H:%M:%S %Z", &ts_out);
    //facture(pass,2);
}

void facture(time_t tp_p,int categorie_id){
    MYSQL *conn;
	MYSQL_RES *res;
	MYSQL_ROW row;

	char *server = "localhost";
	char *user = "root";
	char *password = "password"; /* set me first */
	char *database = "parking";
	char *type[3];
	char *prix_heure[3];
	char *prix_jour[3];
	int prix_p;
	int i;
	int tp_j;
	i=0;
	conn = mysql_init(NULL);
	
	/* Connect to database */
	if (!mysql_real_connect(conn, server, user, password,database, 0, NULL, 0)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
	
	/* send SQL query */ 
	if (mysql_query(conn, "select * from type_engins")) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
   
	res = mysql_store_result(conn);
	
	/* output table name */
	while ((row = mysql_fetch_row(res)) != NULL){
		type[i]=row[0];
		prix_jour[i]=row[3];
		prix_heure[i]=row[4];
		i++;
	}
	printf("le temps passer est de: %ld heures\n",tp_p);
	//prix a paye
	tp_j=tp_p/24;
	printf("nombre de jour passer : %d jours et %ld heures\n",tp_j,tp_p-tp_j*24);
	prix_p=tp_j*(atoi(prix_jour[categorie_id-1]))+(tp_p-tp_j*24)*(atoi(prix_heure[categorie_id-1]));	
	printf("le prix a payer est de : %d\n",prix_p);
	/* close connection */
	mysql_free_result(res);
	mysql_close(conn);


}
