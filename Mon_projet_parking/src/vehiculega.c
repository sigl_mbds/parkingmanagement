/** 
 *@file vehiculega.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include <mysql/mysql.h>
#include <stdio.h>
#include "vehiculega.h"
/** 
 *@brief Fonction qui definit  les vehicules garés
*/

void vehiculega() {
	MYSQL *conn;
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	char *server = "localhost";
	char *user = "fatim";
	char *password = "fanta"; /* set me first */
	char *database = "parkings";
	
	conn = mysql_init(NULL);
	
	/* Connect to database */
	if (!mysql_real_connect(conn, server, user, password,database, 0, NULL, 0)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
	
	/* send SQL query */
	if (mysql_query(conn, "select * from stationner")) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
   
	res = mysql_use_result(conn);
	
	/* output table name */
	printf("\nLa liste des Document:\n");
	printf("\n| matricule | date d'entrer | place occuper |\n"); 
	while ((row = mysql_fetch_row(res)) != NULL)
		printf("| %s | %s | %s |\n", row[1],row[2],row[7]);
   
	/* close connection */
	mysql_free_result(res);
	mysql_close(conn);

}
