/** 
 *@file place_dispo.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include <mysql/mysql.h>
#include <stdio.h>
#include <string.h>
#include "place_dispo.h"
/** 
 *@brief Fonction qui permet de definir les places disponibles
*/

void place_dispo() {
	MYSQL *conn;
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	char *server = "localhost";
	char *user = "fatim";
	char *password = "fanta"; /* set me first */
	char *database = "parkings";
	char resultas[50];
	conn = mysql_init(NULL);
	
	/* Connect to database */
	if (!mysql_real_connect(conn, server, user, password,database, 0, NULL, 0)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
	
	/* send SQL query */
	if (mysql_query(conn, "select * from stationner")) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
   
	res = mysql_use_result(conn);
	char place_parking[][4]={"A1","A2","A3","A4","A5","A6","A7","A8","A9","A10","A11","A12","A13","A14","A15","A16","B1","B2","B3","B4","B5","B6","B7","B8","B9","B10","B11","B12","B13","B14","B15","B16","C1","C2","C3","C4","C5","C6","C7","C8","C9","C10","C11","C12","C13","C14","C15","C16"};
	/* output table name */
	printf("\nLa liste des Document:\n");
	while ((row = mysql_fetch_row(res)) != NULL){
		printf("| %s |\n", row[7]);
        sprintf(resultas,"%s",row[7]);
        // printf("%s",resultas);
        int count=sizeof(place_parking)/sizeof(place_parking[0]);
        for (int k = 0; k < count; k++)
        {   
            printf("%s\t",place_parking[k]);
			
			
        }
    }

	/* close connection */
	mysql_free_result(res);
	mysql_close(conn);
}
