/** 
 *@file entrer.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include<stdio.h>
#include<string.h>

#include<mysql/mysql.h>

#include"mysql_help_function.h"
#include"connexion_options.h"
#include"entrer.h"
/** 
 *@brief Fonction qui gerer les entrees de vehicules
*/

void entrer(int num_porte_entre){

    /*j ajoute un caractere en plus sur la longeur des chaine car toute chaine en langage c se termine par \0*/
    char query_str[400];
    char code_type[3];
    char couleur[11];
    char marque[26];
    char matricule[9];
    char reponse[4];
    MYSQL* conn= connect_to_mysql(HOSTNAME,USERNAME,PASSWORD,PARKING_BD_NAME);

    /*afficher les tarifs*/
    SQL_RES res=execute_query(conn,"select code_type as CODE, lib_type as LIB,tarif_jour as FRAIS_JOUR, tarif_heure as FRAIS_HEURE from type_engins");
    discret_display_result("NOS TARIFS",res);
    free_res(res);

    printf("\n************ ÉTAPE 1 : RECCUEIL D'INFORMATION SUR LE VÉHICULE ************\n");

    /*recuperer le matricule du vehicule*/
    printf(">>Entrez le matricule du vehicule svp!!!\n");
    scanf("%s",matricule);
    tolowercase(matricule);

    /*verifier si le vehicule n'est pas déja garé*/
    sprintf(query_str,"select * from stationner where mat_eng='%s' and date_entrer is not null and date_sortie is null",matricule);
    res=execute_query(conn,query_str);
    if(res.nb_row>0){
        printf("Peut être vous trompé de matricule. Cet véhicule est déja garé dans le parking.\n");
        return;
    }

    /*verifier si le vehicule n est pas déja enregistré*/
    sprintf(query_str,"select E.code_type as CODE_TYPE, lib_type as LIB_TYPE ,mat_eng as MATRICULE, couleur_eng as COULEUR, marque_eng as MARQUE from engins as E, type_engins as T where  E.code_type=T.code_type and mat_eng='%s'",matricule);
    res=execute_query(conn,query_str);

    //si pas encore enregistré, on enregistre
    if(res.nb_row==0){
        
        /*enregistrer le type de vehicule*/
        while(true){
            printf(">> Entrer le code du type de véhicule entrant svp!!!\n");
            scanf("%s",code_type);
            //conversion en miniscule
            tolowercase(code_type);

            //verifier si ce type de vehicule existe
            sprintf(query_str,"select * from type_engins where code_type='%s'",code_type);
            res=execute_query(conn,query_str);
            free_res(res);
            if(res.nb_row==0){
                printf("MAUVAIS CODE !!!\n");
            }
            else break;
        }

        //recuperer la marque et la couleur
        printf(">> Entrez la marque du vehicule svp!!!\n");
        scanf("%s",marque);
        printf(">> Entrez la couleur du vehicule svp!!!\n");
        scanf("%s",couleur);

        //enregistrer le vehicule dans la base de donnée
        sprintf(query_str,"insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ('%s', '%s','%s','%s')",matricule,couleur,marque,code_type);
        //printf("%s\n",query_str);
        res=execute_query(conn,query_str);        
        if(res.state==False){
            printf("\nECHEC D'ENREGISTREMENT DU VEHICULE !!!\n");
            return;
        }
        else{
            printf("\nVEHICULE ENREGISTRÉ AVEC SUCCÈS !!!\n");

        }
        /*free_res(res);*/
    }
    else{
        char lib_type[50];
        printf("VÉHICULE DÉJA ENREGISTRÉ DANS LA BASE DE DONNÉE.\n");
        get_string_value(res,code_type,0);
        get_string_value(res,lib_type,1);
        get_string_value(res,couleur,3);
        get_string_value(res,marque,4);
        printf(">> Type de véhicule : %s\n",lib_type);
        printf(">> Marque du véhicule : %s\n",marque);
        printf(">> Couleur du véhicule : %s\n",couleur);
    }    
    /*verifier s'il y a encore de la place pour ce type de vehicule*/
    //ATTENTION UN VEHICULE classique PEUT STATIONNER A LA PLACE D UN CAMION */

    printf("\n************** ÉTAPE 2 : VÉRIFICATION DE LA DISPONIBILITÉ DE PLACE DU PARKING **************\n\n");

    int nb_place_libre;
    char req_nb_place[]= "select type_engins.nb_place_total-T_nombre_place_occupe.nombre as nombre_place_libre from type_engins,(select count(*) as nombre from stationner where date_entrer is not null and date_sortie is null and code_type_place_occupe='%s')as T_nombre_place_occupe where type_engins.code_type='%s'";
    
    sprintf(query_str,req_nb_place,code_type,code_type);

    res=execute_query(conn,query_str);
    get_int_value(res,&nb_place_libre,0);
    printf("NOMBRE DE PLACE DISPONIBLE: %d\n",nb_place_libre);
    free_res(res);

    //attribuer une place au vehicule
    if(nb_place_libre>0){
        attribuer_place_parking(conn,num_porte_entre,matricule,code_type,code_type);
    }
    else{
        int nb_place_libre_camion=0;

        if(strcmp(code_type,"vc")==0){
            sprintf(query_str,req_nb_place,"ca","ca");
            res=execute_query(conn,query_str);
            get_int_value(res,&nb_place_libre_camion,0);
        }
        if(strcmp(code_type,"vc")!=0 || (strcmp(code_type,"vc")==0  && nb_place_libre_camion<=0)){
            printf("DÉSOLÉ, IL N'Y A PLUS DE PLACE DE PARKING DISPONIBLE !!!\n");
            printf("MERCI ET À BIENTOT!!!\n");
            return;
        }
        else{
            printf("NOMBRE DE PLACE POUR CAMIONS LIBRE : %d\n",nb_place_libre_camion);
            printf("DÉSOLÉ, IL N'Y A PLUS DE PLACE POUR VEHICULE CLASSIQUE DISPONIBLE. SOUHAITEZ-VOUS PRENDRE UNE PLACE POUR CAMION?(OUI/NON)\n");
            scanf("%s",reponse);
            tolowercase(reponse);
            if(strcmp(reponse,"oui")==0){
                attribuer_place_parking(conn,num_porte_entre,matricule,code_type,"CA");
            }
            else{
                printf("MERCI ET À BIENTOT!!!\n");
                return;
            }
        }
    }
}

//pour attribuer une place, il faut d'abord commencer par les places qui sont à la porte d'entrée du vehicule
void attribuer_place_parking(MYSQL *conn,int num_porte_entre,char * matricule_veh,char* type_veh,char * type_place){

    //exemple de requete de creation de table temporaire
    /*
    select distinct * from ( select '14' as code_place union all select "44"  union all select '65' ) as t_liste_place;
    */
    
    char place_vc_entre_1_2[24][4]={"A1","A2","A3","A4","A5","A6","A7","A8","B1","B2","B3","B4","B5","B6","B7","B8","A9","A10","A11","A12","A13","A14","A15","A16"};
    char place_vc_entre_2_1[24][4]={"A9","A10","A11","A12","A13","A14","A15","A16","A1","A2","A3","A4","A5","A6","A7","A8","B1","B2","B3","B4","B5","B6","B7","B8"};
    int nombre_place_vc=24;
    char place_ca_entre_1_2[16][4]={"C1"," C2","C3","C4","C5","C6","C7","C8","B9","B10","B11","B12","B13","B14","B15","B16"};
    char place_ca_entre_2_1[16][4]={"B9","B10","B11","B12","B13","B14","B15","B16","C1"," C2","C3","C4","C5","C6","C7","C8"};
    int nombre_place_ca=16;
    char place_mo_entre_2[9][4]={"C9","C10","C11","C12","C13","C14","C15","C16"};
    int nombre_place_mo=8;


    char query[700];
    char my_query[1000];

    //printf("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
    printf("\n**************** ETAPE 3 : ATTRIBUTION DE PLACE DE PARKING ****************\n");
    printf("------------------------------------------\n");

    printf("TYPE DE VEHICULE : %s\n",type_veh);
    printf("TYPE DE PLACE À ACCORDÉE : %s\n",type_place);
    printf("NUMÉRO DE PORTE D'ENTRÉE : %d\n",num_porte_entre);

    tolowercase(type_veh);
    tolowercase(type_place);


    //toute les place moto sont à l 'entrée 2
    if(strcmp(type_veh,"mo")==0){
        //printf("creation pour moto\n");
        create_sql_temporary_table(query,place_mo_entre_2,nombre_place_mo);
    }
    else if(strcmp(type_veh,"vc")==0){
        //printf("creation pour vc\n");
        if(num_porte_entre==1){
            //le vehicule est à l entrée 1
            //on lui attribue une place de l'entrée 1 en priorité
            create_sql_temporary_table(query,place_vc_entre_1_2,nombre_place_vc);
        }
        else{
             //le vehicule est à l entrée 2
            //on lui attribue une place de l'entrée 2 en priorité
            create_sql_temporary_table(query,place_vc_entre_2_1,nombre_place_vc);
        }
    }
    else if(strcmp(type_veh,"ca")==0){
        //printf("creation pour ca\n");
        if(num_porte_entre==1){
            create_sql_temporary_table(query,place_ca_entre_1_2,nombre_place_ca);
        }
        else{
            create_sql_temporary_table(query,place_ca_entre_2_1,nombre_place_ca);
        }
    }
    else{
        printf("TYPE DE VEHICULE INCORRECT!!!\n");
        return;
    }
    //printf("%s\n",query);
    sprintf(my_query,"select distinct * from (%s) as t_liste_place where code_place not in (select code_place_occupe from stationner where date_entrer is not null and date_sortie is null ) limit 1",query);
    //printf("%s\n",my_query);

   SQL_RES res= execute_query(conn,my_query);

   //display_res_informations(res);
   char code_place[3];
   get_string_value(res,code_place,0);

   sprintf(my_query,"insert into stationner (mat_eng,date_entrer,code_place_occupe,code_type_place_occupe) values ('%s',now(),'%s','%s')",matricule_veh,code_place,type_place);
   res=execute_query(conn,my_query);
   if(res.state==True){
       printf("ENREGISTREMENT ÉFFECTUÉ AVEC SUCCÈS!!!\n");
       printf("PLACE PARKING ATTRIBUÉE: %s\n",code_place);  
   }
   else{
       printf("ÉCHEC DE L'ENREGISTREMENT\n");
   }

    //printf("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
}

void create_sql_temporary_table(char* query,char tab[24][4], int length){
    if(length==0)return;
    //char query[500];
    sprintf(query,"select '%s' as code_place",tab[0]);
    for(int i=1; i<length;i++){
        char c[25];
        sprintf(c," union all select '%s'",tab[i]);
        //printf("%s\n",c);
        strcat(query,c);
    }
    //printf("%s\n",query);


    /*char query[400];
    sprintf(query,"select '%s' as code_place",place_vc_entre1[0]);
    for(int i=1; i<16;i++){
        char c[25];
        sprintf(c," union all select '%s'",place_vc_entre1[i]);
        printf("%s\n",c);
        strcat(query,c);
    }*/
}
