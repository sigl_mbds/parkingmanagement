/** 
 *@file main.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include<stdio.h>
#include<mysql/mysql.h>
#include <unistd.h>//utilisation de commande system
#include"mysql_help_function.h"
#include"connexion_options.h"
#include"entrer.h"
#include"sortie.h"
#include"vehiculecli.h"
#include"vehiculega.h"
#include"place_dispo.h"
/*select UNIX_TIMESTAMP((date_entrer) from stationner where mat_eng='0001ab01' and date_entrer is not null and date_sortie is null;
*/

/** 
 *@brief Fonction principale premettant d'afficher le menu
*/

int main(){
    int choix,num_porte;
    do{
        
        printf("\n***************** MENU *************\n");
      
        printf("1-Entrée de véhicule\n");
        printf("2-Sortie de véhicule\n");
        printf("3-Voir la liste des vehicules clients\n");
        printf("4-Voir la liste des vehicules garés\n");
        printf("5-Afficher les places dispobibles\n");
        printf("7-Quitter le programme\n");
        printf("Faite un choix svp!!!!\n");
        scanf("%d",&choix);
        system("clear");
        switch(choix){
         
            case  1:
                printf("\n**************** ENTRÉE DE VÉHICULE ****************\n\n");
                printf("N° de la porte d'entrée :");
                scanf("%d",&num_porte);
                if(num_porte!=1 && num_porte!=2)printf("MAUVAIS NUMÉRO DE PORTE\n");
                else entrer(num_porte);
                break;
            case  2:
                sortie();
                break;
            case  3:
                //depot();
                vehiculecli();
                break;
            case 4:
                vehiculega();
                //doc_emprsunter();
                break;
            case 5:
                place_dispo();
            	break;
            case  7:
                printf("FIN DU PROGRAMME\n");
                break;
            default:
                printf("CHOIX NON PRIS EN CHARGE!!!\n");
        }
    }
    while(choix!=7);
}
