/** 
 *@file mysql_help_function.c
 *@brief Projet de gestion d'un parking à double entrée
 *@author Groupe 15 
 *@version 1.0
 *@date 29/04/2021
*/

#include <mysql/mysql.h>
#include <stdio.h>
#include <ctype.h>
#include "mysql_help_function.h"
#include "connexion_options.h"

/*
 *AVANT TOUT IL FAUT SE CONNECTER A LA BASE DE DONNEE
 *la fnction prend en parametre les options de connexions et retourne l objet mysql
*/
MYSQL* connect_to_mysql(char* hostname, char* user_name, char *password, char* bd_name ){

    MYSQL *mysql = mysql_init(NULL);
    mysql_options(mysql,MYSQL_READ_DEFAULT_GROUP, "option");

    if (mysql_real_connect(mysql,hostname,user_name ,password, bd_name, 0, NULL, 0) == NULL)
    {
        printf("ERREUR DE CONNEXION: %s \n",mysql_error(mysql));
        mysql_close(mysql);
        return NULL;
    }
    //printf("CONNEXION REUSSI !!!\n");
    return mysql;
}

/*fonction necessaire pour executer une requete*/
SQL_RES execute_query(MYSQL *objet_mysql, char* query_string){

    SQL_RES res;

    if(mysql_query(objet_mysql,query_string) != 0)
    {
        char* message;
        sprintf(message,"%s",mysql_error(objet_mysql));
        res.state=False;
        res.error_message=message;
        printf("Il y a eu une erreur SQL : %s\n", message);
    }
    else
    {
        MYSQL_RES *result = mysql_store_result(objet_mysql);
        res.nb_fields=mysql_field_count(objet_mysql);

        if(result!=NULL)/*il y a des lignes  d informations*/
        {
            MYSQL_FIELD *fields=mysql_fetch_fields(result);
            for(int i=0;i<res.nb_fields;i++){
                /*
                 *on enregistre la liste des champs dans ce tableau. Il en faut au maximum 10
                 *s il y en a plus, changer la taille du tableau dans le fichier d entete
                */
               res.list_fields[i]=fields[i].name;
            }

            res.state=True;
            res.is_select_req=True;
            res.nb_row=mysql_num_rows(result);
            res.result=result;
        }
        else{
            if(res.nb_fields==0){
                /*la requete a reussi mais ce n etait pas un select*/
                res.state=True;
                res.is_select_req=False;
                res.nb_row=mysql_affected_rows(objet_mysql);
            }
            else{
                //la requete a echouée
                res.state=False;
                char* message;
                sprintf(message,"%s",mysql_error(objet_mysql));
                res.error_message=message;
                printf("ERREUR: %s\n",message);
            }
        }
    }

    return res;
}

/*AFFICHAGE DE RESULTAT POUR UN SELECT*/
/** 
 *@brief Fonction permettant d'afficher le resultat à partir d'un select
*/

void display_result(char* title, SQL_RES res){

    MYSQL_ROW row;
    char * head;

    //on affiche les donnees d un requete select uniquement
    if(res.is_select_req==False){
        printf("\n**AUCUNE LISTE A AFFICHER!!!\n");
        return;
    }

    printf("\n*************************************************************************\n");
    printf("\n                       %s                   \n",title);
    printf("\n*************************************************************************\n\n");

    /*S IL N Y A AUCUNE DONNEE LE SIGNIFIER*/
    if(res.nb_row==0){
        printf("LISTE VIDE!!!\n");
        printf("*************************************************************************\n");

        return;
    } 

    /*AFFICHAGE DES CHAMPS*/
    //MYSQL_FIELD *fields=mysql_fetch_fields(res.result);
    //printf("*************************************************************************\n\n");
    for(int i=0;i<res.nb_fields;i++){
        printf("[%s] ",res.list_fields[i]);
    }
    printf("\n--------------------------------------------------------------\n");

    //On choisit une ligne.
    mysql_data_seek(res.result, 0);//pour s'assurer que le curseur est a la ligne qu on voudrait
    while ( (row = mysql_fetch_row(res.result)) ) {
        //On déclare un pointeur long non signé pour y stocker la taille des valeurs
        unsigned long *lengths = mysql_fetch_lengths(res.result);
        for(int i = 0; i < res.nb_fields; i++){
            printf("[%.*s] ", (int) lengths[i], row[i] ? row[i] : "NULL");
        }
        printf("\n");
    }
    printf("\n*************************************************************************\n\n");
}

/*AFFICHAGE DE RESULTAT POUR UN SELECT*/
void discret_display_result(char* title, SQL_RES res){

    MYSQL_ROW row;
    char * head;

    //on affiche les donnees d un requete select uniquement
    if(res.is_select_req==False){
        printf("\n>>AUCUNE LISTE A AFFICHER!!!\n");
        return;
    }

    printf("\n**********************%s**********************\n",title);

    /*S IL N Y A AUCUNE DONNEE LE SIGNIFIER*/
    if(res.nb_row==0){
        printf(">>LISTE VIDE!!!\n");
        return;
    } 

    for(int i=0;i<res.nb_fields;i++){
        printf("[%s] ",res.list_fields[i]);
    }
    printf("\n----------------------------------------------\n");

    //On choisit une ligne.
    mysql_data_seek(res.result, 0);//pour s'assurer que le curseur est a la ligne qu on voudrait
    while ( (row = mysql_fetch_row(res.result)) ) {
        //On déclare un pointeur long non signé pour y stocker la taille des valeurs
        unsigned long *lengths = mysql_fetch_lengths(res.result);
        for(int i = 0; i < res.nb_fields; i++){
            printf("[%.*s] ", (int) lengths[i], row[i] ? row[i] : "NULL");
        }
        printf("\n");
    }
   // printf("\n*************************************************************************\n\n");
}

/*obtenir la premiere information sous forme d entier
//fonction ideale pour les requete qui retourne un nombre entier info comme les count et max*/
/** 
 *@brief Fonction ideale pour les requete qui retourne un nombre entier info comme les count et max(pour entier)
*/

void get_int_value(SQL_RES res, int *nombre,int value_index){
    MYSQL_ROW row;
    //On choisit une ligne.
    mysql_data_seek(res.result, 0);
    //On déclare un pointeur long non signé pour y stocker la taille des valeurs

    while ( (row = mysql_fetch_row(res.result)) ) {
        *nombre=atoi(row[value_index]);
        break;
    }
}

/*obtenir la premiere information  sous forme de chaine
//fonction ideale pour les requete qui retourne un nombre entier info comme les count et max*/
/** 
 *@brief Fonction pour les requete qui retourne un nombre entier info comme les count et max(pour les chaines de caracteres)
*/


void get_string_value(SQL_RES res, char *chaine,int value_index){
    MYSQL_ROW row;
    //On choisit une ligne.
    mysql_data_seek(res.result, 0);
    //On déclare un pointeur long non signé pour y stocker la taille des valeurs

    while ( (row = mysql_fetch_row(res.result)) ) {
        sprintf(chaine,"%s",row[value_index]);
        break;
    }
}
/** 
 *@brief Fonction pour les requete qui retourne un nombre entier info comme les count et max (pour reel)
*/
void get_float_value(SQL_RES res, float *valeur,int value_index){
    MYSQL_ROW row;
    //On choisit une ligne.
    mysql_data_seek(res.result, 0);
    //On déclare un pointeur long non signé pour y stocker la taille des valeurs

    while ( (row = mysql_fetch_row(res.result)) ) {
        sscanf(row[value_index],"%f",valeur);
        break;
    }
}

/*ne pas oublier de liberer une ressource dont on a plus besoin*/
void free_res(SQL_RES res){
    mysql_free_result(res.result);
}

/*fermer la connection*/
void close_connection(MYSQL *object){
    mysql_close(object);
}


/*afficher les info sur le resultat d une requete*/
void display_res_informations(SQL_RES res){
    printf("\n**********DETAILS SUR LE RESULTAT SQL_RES************\n");
    char * sta=res.state==True?"REQUETE REUSSI":"ECHEC DE LA REQUETE";
    printf("\n**STATUT : %s\n",sta);

    if(res.state==False){
        printf("\n**MESSAGE D'ERREUR : %s\n",res.error_message);
    }
    else{
        printf("\n**TYPE : %s\n",res.is_select_req==True?"SELECT":"AUTRE");
        if(res.is_select_req==True){
            printf("\n**NOMBRE DE LIGNE : %d\n",res.nb_row);
            printf("\n**NOMBRE DE CHAMP : %d\n",res.nb_fields);
        }
        else{

        }
    }
    
}
/**/
void tolowercase(char* chaine){
    int i=0;
    while(chaine[i]!='\0'){
        chaine[i]=tolower(chaine[i]);
        i++;
    }

    /*
        char chaine[]="SRDTEDSTFYAGVZIHBN?%lmmLZKJH DFDZTFYGUJMEÙAZOEI,UNDGNQ,D;QDJHFYUGHIJ";
        tolowercase(chaine);
        printf("%s\n",chaine);

        tolowercase(code_type);
        printf("%s\n",code_type);
    */
}
