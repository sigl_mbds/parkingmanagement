/*POUR EXECUTER CE FICHIER DANS MYSQL, SYNTAXE: source CHEMIN_ACCES;*/
drop database if exists parkings;
create database parkings character set "utf8" ;
use parkings;

create table type_engins(
    code_type varchar(2) primary key, 
    lib_type varchar(25) not null unique,
    nb_place_total smallint unsigned not null, 
    tarif_jour float unsigned not null,
    tarif_heure float unsigned not null
); 

create table engins(
    mat_eng varchar(8) primary key  not null , 
    num_eng smallint unsigned unique not null auto_increment,
    couleur_eng varchar(10) not null, 
    marque_eng varchar(25) not null,
    code_type varchar(02) not null
);   
alter table engins add constraint fk_type foreign key(code_type) references type_engins(code_type);

create table stationner(
    id_stat smallint unsigned primary key auto_increment, 
    mat_eng varchar(8) not null,
    date_entrer datetime not null, 
    date_sortie datetime , 
    montant_facture float unsigned , 
    duree_total varchar(25) , 
    code_type_place_occupe varchar(2) not null, 
    code_place_occupe varchar(5) not null 
);
alter table stationner add constraint fk_eng foreign key(mat_eng) references engins(mat_eng);

insert into type_engins (code_type,lib_type,nb_place_total,tarif_jour,tarif_heure) values ("VC","VOITURE CLASSIQUE",24,20000,500);
insert into type_engins (code_type,lib_type,nb_place_total,tarif_jour,tarif_heure) values ("CA","CAMION",16,30000,700);
insert into type_engins (code_type,lib_type,nb_place_total,tarif_jour,tarif_heure) values ("MO","MOTO",8,10000,300);

insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ("0001ab01","Rose","Peugeot","vc");
insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ("0002ab01","Vert","Opel","mo");
insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ("0003ab01","Bleu","Mercedes","ca");
insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ("0004ab01","Marron","Peugeot","vc");
insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ("0005ab01","Orange","Opel","mo");
insert into engins (mat_eng,couleur_eng,marque_eng,code_type) values ("0006ab01","Gris","Mercedes","ca");

show tables;
describe type_engins;
describe engins;
describe stationner;
select * from stationner;
select * from type_engins;
select * from engins;

