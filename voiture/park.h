#include "/opt/lampp/include/mysql.h"

#ifndef _PARK_H_
#define _PARK_H_

#ifdef park_IMPORT
    #define EXTERN
#else 	 
    #define EXTERN extern  
#endif 

EXTERN void create();
EXTERN void finish_with_error(MYSQL *c);    
    
   
#undef EXTERN
#endif