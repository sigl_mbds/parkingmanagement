# GESTION DE PARKING EN LANGUAGE C

## Objectifs du projet
    -[x]  Maitrise de la programation avancée en language C
    -[x]  Mise en place de file d'attente avec le language C
    -[x]  comprehension de la compilation d'un programme en C ave gcc et à  partir d'un fichier Makefile
    -[x]  Mise en place de librairie static et dynamic en C
    -[x]  Programmation modulaire en c
    -[x]  Connexion de C avec une base de données MYSQL

## Outils utilisés pour ce projet
    -  Language C
    -  Base données MYSQL 
    -  Serveur XAMPP
    -  Compilateur gcc

## Membres du groupe 
    - N'DRI KOUAME DAVID (SIGL 1)
    - KAMATE FANTA (SIGL 1)
    - ABLIN KOUAKOU KOFFI PATERNE (MBDS 1)


## Superviseur

    DOCTEUR DIABY MOUSTAPHA ENSEIGNANT CHERCHEUR A ESATIC


## Note du groupe
        /20
## Observation du superviseur
