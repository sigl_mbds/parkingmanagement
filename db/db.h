# include <stdio.h>
# include <stdlib.h>
#include <mysql/mysql.h>



#ifdef DEFINE_CON
MYSQL *con;
#else
extern MYSQL *con; 
#endif

#ifndef DB_H
#define DB_H
 
#ifdef DB_IMPORT
    #define EXTERN
#else 	 
    #define EXTERN extern 
#endif 
  

EXTERN int db();


 
#undef EXTERN
#endif